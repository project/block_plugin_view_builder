CONTENTS OF THIS FILE
---------------------
* Summary
* Usage
* Requirements
* Maintainers

SUMMARY
--------------
The **Block plugin view builder** module allows developers to
programmatically render (view) block plugins without the need for
the wrapping BlockContent entity. This module is an adaptation of the
core BlockViewBuilder and makes sure that:

- Each block gets a cache key, so it can be cached in the render cache
- Cacheable metadata (including access metadata) is taken into account
- Block plugins get a lazybuilder callback so they
  can be correctly placeholdered

This all results in better leveraging of the dynamic page cache.

**Note:** This module does not aim to implement rendering
or viewing of BlockContent blocks as these
should best be viewed using the core BlockViewBuilder.

USAGE
--------

    $blockPluginViewBuilder = \Drupal::service('block_plugin.view_builder');
    // Pass optional configuration.
    $configuration = [];
    $build = $blockPluginViewBuilder->view(‘plugin_id', $configuration);

REQUIREMENTS
------------
- php >= 7.4
- Drupal >= 8.8

MAINTAINERS
-----------
Current maintainers:
* Wouter Vanelslander - https://www.drupal.org/u/wtrv
