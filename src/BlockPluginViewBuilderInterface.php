<?php

namespace Drupal\block_plugin_view_builder;

use Drupal\Core\Block\BlockPluginInterface;

/**
 * Provides an interface for block plugin view builders.
 */
interface BlockPluginViewBuilderInterface {

  /**
   * Renders a block plugin from a given plugin Id.
   *
   * @param string $pluginId
   *   The plugin id.
   * @param array $configuration
   *   Optional configuration.
   *
   * @return array
   *   The block render array.
   */
  public function view(string $pluginId, array $configuration = []): array;

  /**
   * Renders a block plugin.
   *
   * @param \Drupal\Core\Block\BlockPluginInterface $plugin
   *   The block plugin to render.
   *
   * @return array
   *   The block render array.
   */
  public function viewPlugin(BlockPluginInterface $plugin);

}
