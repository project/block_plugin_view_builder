<?php

namespace Drupal\block_plugin_view_builder;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Block\TitleBlockPluginInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\TitleResolverInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a service for building block plugins without a block entity.
 */
class BlockPluginViewBuilder implements TrustedCallbackInterface, BlockPluginViewBuilderInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $account;

  /**
   * The block plugin manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected BlockManagerInterface $blockManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The context repository.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected ContextRepositoryInterface $contextRepository;

  /**
   * The context handler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandlerInterface
   */
  protected ContextHandlerInterface $contextHandler;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The title resolver.
   *
   * @var \Drupal\Core\Controller\TitleResolverInterface
   */
  protected TitleResolverInterface $titleResolver;

  /**
   * BlockPluginViewBuilder constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Block\BlockManagerInterface $blockManager
   *   The block manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $contextRepository
   *   The context repository.
   * @param \Drupal\Core\Plugin\Context\ContextHandlerInterface $contextHandler
   *   The context handler.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   * @param \Drupal\Core\Controller\TitleResolverInterface $titleResolver
   *   The title resolver.
   */
  public function __construct(
    AccountInterface $account,
    BlockManagerInterface $blockManager,
    ModuleHandlerInterface $moduleHandler,
    ContextRepositoryInterface $contextRepository,
    ContextHandlerInterface $contextHandler,
    RequestStack $requestStack,
    RouteMatchInterface $routeMatch,
    TitleResolverInterface $titleResolver
  ) {
    $this->account = $account;
    $this->blockManager = $blockManager;
    $this->moduleHandler = $moduleHandler;
    $this->contextRepository = $contextRepository;
    $this->contextHandler = $contextHandler;
    $this->requestStack = $requestStack;
    $this->routeMatch = $routeMatch;
    $this->titleResolver = $titleResolver;
  }

  /**
   * {@inheritDoc}
   */
  public function view(string $pluginId, array $configuration = []): array {
    /** @var \Drupal\Core\Block\BlockPluginInterface $plugin */
    $plugin = $this->blockManager->createInstance($pluginId, $configuration);
    return $this->viewPlugin($plugin);
  }

  /**
   * {@inheritDoc}
   */
  public function viewPlugin(BlockPluginInterface $plugin) {
    $build = [];

    // Inject runtime contexts.
    if ($plugin instanceof ContextAwarePluginInterface) {
      $contexts = $this->contextRepository->getRuntimeContexts($plugin->getContextMapping());
      $this->contextHandler->applyContextMapping($plugin, $contexts);
    }

    // Gather access cacheable metadata.
    $access = $plugin->access($this->account, TRUE);
    $cacheableMetadataAccess = CacheableMetadata::createFromObject($access);

    if ($access->isAllowed()) {
      $encodedConfiguration = \json_encode($plugin->getConfiguration());

      // Create the render array for the block as a whole.
      // @see template_preprocess_block().
      $build[$plugin->getPluginId()] = [
        '#cache' => [
          'keys' => [
            'block_plugin_view_builder',
            $plugin->getPluginId(),
            '[configuration]=' . \hash('sha256', $encodedConfiguration),
          ],
          'contexts' => $plugin->getCacheContexts(),
          'tags' => Cache::mergeTags(['block_view'], $plugin->getCacheTags()),
          'max-age' => $plugin->getCacheMaxAge(),
        ],
      ];

      $this->moduleHandler->alter([
        'block_build',
        "block_build_" . $plugin->getBaseId(),
      ], $build[$plugin->getPluginId()], $plugin);

      if ($plugin instanceof TitleBlockPluginInterface) {
        $request = $this->requestStack->getCurrentRequest();
        $title = $this->titleResolver->getTitle($request, $this->routeMatch->getRouteObject());
        $plugin->setTitle($title);

        // Immediately build a #pre_render-able block,
        // since this block cannot be built lazily.
        $build[$plugin->getPluginId()] += BlockPluginViewBuilder::buildPreRenderableBlock($plugin, $this->moduleHandler);
        $build[$plugin->getPluginId()]['#cache']['contexts'][] = 'url';
      }
      else {
        // Assign a #lazy_builder callback,
        // which will generate a #pre_render-able
        // block lazily (when needed).
        $build[$plugin->getPluginId()] += [
          '#lazy_builder' => [
            'block_plugin.view_builder:lazyBuilder',
            [$plugin->getPluginId(), $encodedConfiguration],
          ],
        ];
      }
    }

    $cacheableMetadataAccess->applyTo($build);

    return $build;
  }

  /**
   * The #pre_render callback for building a block.
   *
   * Renders the content using the provided block plugin, and then:
   * - if there is no content, aborts rendering, and makes sure the block won't
   *   be rendered.
   * - if there is content, moves the contextual links from the block content to
   *   the block itself.
   *
   * @param array $build
   *   The block.
   *
   * @return array
   *   The render array.
   *
   * @see BlockViewbuilder::preRender()
   */
  public static function preRender(array $build): array {
    /** @var \Drupal\Core\Block\BlockPluginInterface $plugin */
    $plugin = $build['#block'];
    $content = $plugin->build();

    unset($build['#block']);

    if ($content !== NULL && !Element::isEmpty($content)) {
      foreach (['#attributes', '#contextual_links'] as $property) {
        if (isset($content[$property])) {
          if (!isset($build[$property])) {
            $build[$property] = [];
          }

          $build[$property] += $content[$property];
          unset($content[$property]);
        }
      }

      $build['content'] = $content;
    }
    else {
      $build = [
        '#markup' => '',
        '#cache' => $build['#cache'],
      ];

      if (!empty($content)) {
        CacheableMetadata::createFromRenderArray($build)
          ->merge(CacheableMetadata::createFromRenderArray($content))
          ->applyTo($build);
      }
    }

    return $build;
  }

  /**
   * Lazy_builder callback; builds a #pre_render-able block.
   *
   * @param string $blockPluginId
   *   The block plugin id.
   * @param string $configuration
   *   The json encoded block plugin configuration.
   *
   * @return array
   *   A render array with a #pre_render callback to render the block.
   */
  public function lazyBuilder(string $blockPluginId, string $configuration = ''): array {
    $configuration = \json_decode($configuration, TRUE) ?? [];

    /** @var \Drupal\Core\Block\BlockPluginInterface $blockPlugin */
    $blockPlugin = $this->blockManager->createInstance($blockPluginId, $configuration);
    return static::buildPreRenderableBlock($blockPlugin, $this->moduleHandler);
  }

  /**
   * Builds a #pre_render-able block render array.
   *
   * @param \Drupal\Core\Block\BlockPluginInterface $plugin
   *   A block plugin.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   *
   * @return array
   *   A render array of the block plugin.
   */
  public static function buildPreRenderableBlock(BlockPluginInterface $plugin, ModuleHandlerInterface $moduleHandler): array {
    // Inject runtime contexts.
    if ($plugin instanceof ContextAwarePluginInterface) {
      $contexts = \Drupal::service('context.repository')->getRuntimeContexts($plugin->getContextMapping());
      \Drupal::service('context.handler')->applyContextMapping($plugin, $contexts);
    }

    $build = [
      '#theme' => 'block',
      '#id' => $plugin->getPluginId(),
      '#attributes' => [],
      '#configuration' => $plugin->getConfiguration(),
      '#plugin_id' => $plugin->getPluginId(),
      '#base_plugin_id' => $plugin->getBaseId(),
      '#derivative_plugin_id' => $plugin->getDerivativeId(),
      '#pre_render' => [
        static::class . '::preRender',
      ],
      '#block' => $plugin,
    ];

    $baseId = $plugin->getBaseId();
    $moduleHandler->alter(['block_view', "block_view_$baseId"], $build, $plugin);

    return $build;
  }

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks(): array {
    return ['lazyBuilder', 'preRender'];
  }

}
